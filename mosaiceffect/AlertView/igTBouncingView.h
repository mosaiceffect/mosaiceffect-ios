//
//  igTraining
//
//  Created by Upakul on 15/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface igTBouncingView : UIView

@property (nonatomic, strong) UIImageView* image;

- (id)initSuccessCircleWithFrame:(CGRect)frame andImageSize:(int) imageSize andColor:(UIColor*) color;
- (CGRect) newFrameWithWidth:(float) width andHeight:(float) height;

@end
