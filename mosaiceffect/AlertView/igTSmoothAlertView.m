//
//  igTraining
//
//  Created by Upakul on 15/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import "igTSmoothAlertView.h"
#import "UIImage+EMAdditions.h"
#import "IGString.h"
#import "PhotoScreenVC.h"

@implementation igTSmoothAlertView
{
    UIView *alertView;
    igTBouncingView *circleView;
    UIImageView * bg;
    
}

- (id) initDropAlertWithTitle:(NSString*) title andText:(NSString*) text andCancelButton:(BOOL)hasCancelButton andColor:(UIColor*) color andImage:(UIImage*)image andFont:(UIFont *)font
{
    self = [super init];
    if (self) {
        // Initialization code
        [self _initViewWithTitle:title andText:text andCancelButton:hasCancelButton andColor:color andImage:image andFont:font];
    }
    return self;
}

- (id) initDropAlertWithTitle:(NSString*) title andText:(NSString*) text andCancelButton:(BOOL)hasCancelButton andColor:(UIColor*) color andImage:(UIImage*)image withCompletionHandler:(dismissAlertWithButton)completionHandler formAlert:(BOOL)alert
{
    self = [super init];
    if (self) {
        // Initialization code
        self.isFormAlert = alert;
        self.completionBlock = completionHandler;
        [self _initViewWithTitle:title andText:text andCancelButton:hasCancelButton andColor:color andImage:image andFont:nil];
    }
    return self;
}

- (void) _initViewWithTitle:(NSString *)title andText:(NSString *)text andCancelButton:(BOOL)hasCancelButton  andColor:(UIColor*) color andImage:(UIImage*)image andFont:(UIFont *)font
{
    self.frame = [self screenFrame];
    self.opaque = YES;
    self.alpha = 1;
        
    bg = [[UIImageView alloc]initWithFrame:[self screenFrame]];
    
    alertView = [self alertPopupView];
    
    [self labelSetupWithTitle:title andText:text];
    
    [self buttonSetupWithCancelButton: hasCancelButton andColor:color andFont:font];
    [self addSubview:alertView];
    
    [self circleSetupForColor:color andImage:image];
}


- (UIView*) alertPopupView
{
    UIView * alertSquare = nil;
    
    if (self.isFormAlert) {
        alertSquare = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 150)];
    }
    else {
        alertSquare = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 200)];
    }
    
    alertSquare.backgroundColor = [UIColor colorWithRed:0.937 green:0.937 blue:0.937 alpha:1];
    alertSquare.center = CGPointMake([self screenFrame].size.width/2, -[self screenFrame].size.height/2);
     //shipra removed corner radius
    alertSquare.layer.cornerRadius = 5.0f;
    [self setCornerRadius:20.0f];
    
    [alertSquare.layer setShadowColor:[UIColor blackColor].CGColor];
    [alertSquare.layer setShadowOpacity:0.4];
    [alertSquare.layer setShadowRadius:20.0f];
    [alertSquare.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    
    [self performScreenshotAndBlur];
    
    return alertSquare;
}


- (void)show
{
	id<EMSmoothAlertViewDelegate> delegate = self.delegate;
	if ([delegate respondsToSelector:@selector(alertViewWillShow:)]) [delegate alertViewWillShow:self];
    [self triggerDropAnimations];
    [[[UIApplication sharedApplication] keyWindow] addSubview:self];
    if (self.isFormAlert) {
        [_textField becomeFirstResponder];
    }
}

#pragma mark - Blur

-(void) performScreenshotAndBlur
{
    UIImage * image = [self convertViewToImage];    
    UIImage *blurredViewSnapshot = [image
                                    emApplyBlurWithRadius:5.0f
                                    tintColor:[UIColor colorWithWhite:0.0f alpha:0.5f]
                                    saturationDeltaFactor:1.0f
                                    maskImage:nil];
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:blurredViewSnapshot];
    backgroundView.frame = self.bounds;
    backgroundView.alpha = 0.0f;

    [bg setImage:backgroundView.image];
    
    [self addSubview:bg];
}


#pragma mark - Items Setup

- (void)circleSetupForColor:(UIColor*) color andImage:(UIImage*)image
{
    CGFloat width = CGRectGetWidth([self screenFrame]) / 2;
    CGFloat height =  (CGRectGetHeight([self screenFrame]) - CGRectGetHeight(alertView.frame)) / 2;
    UIView * circleMask = [[UIView alloc]initWithFrame:CGRectMake(width, height, 60, 60)];
    [self addSubview:circleMask];

    circleView = [[igTBouncingView alloc] initSuccessCircleWithFrame:CGRectMake(0, 0, 0, 0) andImageSize:60 andColor:color];
    _logoView = [[UIImageView alloc]initWithFrame:CGRectMake(circleMask.frame.size.width/2-30, circleMask.frame.size.height/2-30 , 0, 0)];
    [_logoView setImage:image];
    _logoView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [circleMask addSubview:circleView];
    [circleMask addSubview:_logoView];
}

- (void) labelSetupWithTitle:(NSString*) title andText:(NSString*) text
{
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 230, 40)];
    _titleLabel.center = CGPointMake(alertView.frame.size.width/2, 50);
    _titleLabel.text = title;
    _titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:18.0f];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [alertView addSubview:_titleLabel];
    
    if (self.isFormAlert) {
        _textField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 220, 40)];
        _textField.center = CGPointMake(alertView.frame.size.width/2, 80);
        _textField.placeholder = text;
        _textField.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.keyboardType = UIKeyboardTypeEmailAddress;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.returnKeyType = UIReturnKeyDone;
        _textField.delegate = self;
        [alertView addSubview:_textField];
    }
    else {
    
        _textLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 230, 60)];
        _textLabel.center = CGPointMake(alertView.frame.size.width/2, 100);
        _textLabel.text = text;
        _textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0f];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _textLabel.numberOfLines = 0;
        [alertView addSubview:_textLabel];
    }
}

- (void) buttonSetupWithCancelButton:(BOOL) hasCancelButton andColor:(UIColor*) color andFont:(UIFont *)fontWithSize
{
    
    if (hasCancelButton) {
        //default button
        _defaultButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 104, 30)];
        
        if (self.isFormAlert) {
            _defaultButton.center = CGPointMake((alertView.frame.size.width*3/4)-3, 120);

            //[_defaultButton setUserInteractionEnabled:FALSE];

            [_defaultButton setUserInteractionEnabled:TRUE];
        }
        else
            _defaultButton.center = CGPointMake((alertView.frame.size.width*3/4)-3, 160);
        
        //cancel button
        _cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 104, 30)];
        if (self.isFormAlert) {
            _cancelButton.center = CGPointMake((alertView.frame.size.width/4)+3, 120);
        }
        else
        _cancelButton.center = CGPointMake((alertView.frame.size.width/4)+3, 160);
        _cancelButton.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1];
        
        [_cancelButton setTitle:KTakeAnotherSelfie forState:UIControlStateNormal];
        _cancelButton.titleLabel.textColor = color;
        _cancelButton.titleLabel.font = fontWithSize;
		[_cancelButton addTarget:self action:@selector(takeAnotherSelfieTouched:) forControlEvents:UIControlEventTouchUpInside];
         //shipra removed corner radius
        [_cancelButton.layer setCornerRadius:3.0f];
    }
    
    else{

        _defaultButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 180, 30)];
        
        if (self.isFormAlert) {
            _defaultButton.center = CGPointMake(alertView.frame.size.width/2, 120);
            [_defaultButton setUserInteractionEnabled:FALSE];
        }
        else
            _defaultButton.center = CGPointMake(alertView.frame.size.width/2, 160);
    }
    
    
    _defaultButton.backgroundColor = color;
    
    //default button end setup
    [_defaultButton setTitle:kStringOK forState:UIControlStateNormal];
    _defaultButton.titleLabel.font = fontWithSize;
	[_defaultButton addTarget:self action:@selector(handleButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
     //shipra removed corner radius
    [_defaultButton.layer setCornerRadius:3.0f];
    
    [alertView addSubview:_defaultButton];
    if (hasCancelButton)[alertView addSubview:_cancelButton];
    
}



- (void) setTitleFont:(UIFont *)titleFont
{
    [_titleLabel setFont:titleFont];
}

- (void) setTextFont:(UIFont *)textFont
{
    [_textLabel setFont:textFont];
}

-(void) setTitleText:(NSString*) string
{
    _titleLabel.text = string;
}

-(void) setMessageText:(NSString*) string
{
    _textLabel.text = string;
}

#pragma mark - Animations

-(void) triggerDropAnimations
{
    
    NSMutableArray* animationBlocks = [NSMutableArray new];
    
    typedef void(^animationBlock)(BOOL);
    
    // getNextAnimation
    // removes the first block in the queue and returns it
    animationBlock (^getNextAnimation)() = ^{
        animationBlock block = animationBlocks.count ? (animationBlock)[animationBlocks objectAtIndex:0] : nil;
        if (block){
            [animationBlocks removeObjectAtIndex:0];
            return block;
        }else{
            return ^(BOOL finished){};
        }
    };
    
    //block 1
    [animationBlocks addObject:^(BOOL finished){;
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            bg.alpha = 1.0;
        } completion: getNextAnimation()];
    }];
    
    //block 2
    [animationBlocks addObject:^(BOOL finished){;
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            alertView.center = CGPointMake([self screenFrame].size.width/2, ([self screenFrame].size.height/2)+alertView.frame.size.height*0.1);
        } completion: getNextAnimation()];
    }];
    
    //block 3
    [animationBlocks addObject:^(BOOL finished){;
        [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            alertView.center = CGPointMake([self screenFrame].size.width/2, ([self screenFrame].size.height/2));
        } completion: getNextAnimation()];
    }];
    
    //add a block to our queue
    [animationBlocks addObject:^(BOOL finished){;
        [self circleAnimation];
    }];
    
    // execute the first block in the queue
    getNextAnimation()(YES);
}

- (void) circleAnimation
{
    NSMutableArray* animationBlocks = [NSMutableArray new];
    
    typedef void(^animationBlock)(BOOL);
    
    // getNextAnimation
    // removes the first block in the queue and returns it
    animationBlock (^getNextAnimation)() = ^{
        animationBlock block = animationBlocks.count ? (animationBlock)[animationBlocks objectAtIndex:0] : nil;
        if (block){
            [animationBlocks removeObjectAtIndex:0];
            return block;
        }else{
            return ^(BOOL finished){};
        }
    };
    
    //block 1
    [animationBlocks addObject:^(BOOL finished){;
        [UIView animateWithDuration:0.15 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            circleView.frame = [circleView newFrameWithWidth:85 andHeight:85];
            _logoView.frame = [self newFrameForView:_logoView withWidth:40 andHeight:40];
        } completion: getNextAnimation()];
    }];
    
    //block 2
    [animationBlocks addObject:^(BOOL finished){;
        [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            circleView.frame = [circleView newFrameWithWidth:50 andHeight:50];
            _logoView.frame = [self newFrameForView:_logoView withWidth:30 andHeight:30];
        } completion: getNextAnimation()];
    }];
    
    //block 3
    [animationBlocks addObject:^(BOOL finished){;
        [UIView animateWithDuration:0.05 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            circleView.frame = [circleView newFrameWithWidth:60 andHeight:60];
            _logoView.frame = [self newFrameForView:_logoView withWidth:40 andHeight:40];
        } completion: getNextAnimation()];
    }];
    
    //add a block to our queue
    [animationBlocks addObject:^(BOOL finished){;
        self.isDisplayed = true;
		id<EMSmoothAlertViewDelegate> delegate = self.delegate;
		if ([delegate respondsToSelector:@selector(alertViewDidShow:)]) [delegate alertViewDidShow:self];
    }];
    
    // execute the first block in the queue
    getNextAnimation()(YES);
}


- (void) dismissAlertView
{
	id<EMSmoothAlertViewDelegate> delegate = self.delegate;
	if ([delegate respondsToSelector:@selector(alertViewWillDismiss:)]) [delegate alertViewWillDismiss:self];
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                         self.isDisplayed = false;
						 if ([delegate respondsToSelector:@selector(alertViewDidDismiss:)]) [delegate alertViewDidDismiss:self];
                     }];
}


#pragma mark - Miscellaneous

- (void) setCornerRadius:(float)cornerRadius
{
    [alertView.layer setCornerRadius:cornerRadius];
}


- (CGRect) screenFrame
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return screenRect;
}


-(UIImage *)convertViewToImage
{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    CGRect rect = [keyWindow bounds];
    UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [keyWindow.layer renderInContext:context];
    UIImage *capturedScreen = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return capturedScreen;
}

- (CGRect) newFrameForView:(UIView*) uiview withWidth:(float) width andHeight:(float) height
{
    return CGRectMake(uiview.frame.origin.x + ((uiview.frame.size.width - width)/2),
                      uiview.frame.origin.y + ((uiview.frame.size.height - height)/2),
                      width,
                      height);
}

#pragma mark - Delegate Methods
- (void)handleButtonTouched:(id)sender {
	[self dismissAlertView];
    
	/*id<EMSmoothAlertViewDelegate> delegate = self.delegate;
	UIButton *button = (UIButton *) sender;
	if ([delegate respondsToSelector:@selector(alertView:didDismissWithButton:)]) {
		// Since there isn't a button index for the alertView, pass the button
		[delegate alertView:self didDismissWithButton:button];
	}
    
    if(self.completionBlock) {
        self.completionBlock(self, button);
    }*/

}

-(void)takeAnotherSelfieTouched:(id)sender{
    [self dismissAlertView];
    id<EMSmoothAlertViewDelegate> delegate = self.delegate;
    UIButton *button = (UIButton *) sender;
    PhotoScreenVC*vc;
    [vc actionBackButton];
    
    if ([delegate respondsToSelector:@selector(alertView:didDismissWithButton:)]) {
        // Since there isn't a button index for the alertView, pass the button
        [delegate alertView:self didDismissWithButton:button];
        
    }
    
    if(self.completionBlock) {
        self.completionBlock(self, button);
    }

}

#pragma mark Text Field
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string   // return NO to not change text
{
    NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"@"];
    NSRange crange = [[_textField text] rangeOfCharacterFromSet:cset];
    if (crange.location != NSNotFound) {
        // @ is present
        [_defaultButton setUserInteractionEnabled:TRUE];
    }
    return YES;
}

@end
