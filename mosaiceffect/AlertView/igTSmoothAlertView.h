//
//  igTraining
//
//  Created by Upakul on 15/01/15.
//  Copyright (c) 2015 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "igTBouncingView.h"

@class igTSmoothAlertView;

typedef void (^dismissAlertWithButton)(igTSmoothAlertView *, UIButton *);

@protocol EMSmoothAlertViewDelegate;

@interface igTSmoothAlertView : UIView <UITextFieldDelegate>


@property (nonatomic, assign) float cornerRadius;
@property (nonatomic, assign) bool isDisplayed;
@property (nonatomic, assign) BOOL isFormAlert;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * textLabel;
@property (nonatomic, strong) UITextField * textField;
@property (nonatomic, strong) UIButton *defaultButton;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIImageView *logoView;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic, strong) UIFont *textFont;
@property (nonatomic, weak) id<EMSmoothAlertViewDelegate> delegate;
@property (nonatomic, copy) dismissAlertWithButton completionBlock;


- (id) initDropAlertWithTitle:(NSString*) title andText:(NSString*) text andCancelButton:(BOOL)hasCancelButton andColor:(UIColor*) color andImage:(UIImage*)image andFont: (UIFont *)font;
- (id) initDropAlertWithTitle:(NSString*) title andText:(NSString*) text andCancelButton:(BOOL)hasCancelButton andColor:(UIColor*) color andImage:(UIImage*)image withCompletionHandler:(dismissAlertWithButton)completionHandler formAlert:(BOOL)alert;

- (void) setCornerRadius:(float)cornerRadius;
- (void) setTitleText:(NSString*) string;
- (void) setMessageText:(NSString*) string;
- (void) show;
- (void) dismissAlertView;
- (void) handleButtonTouched:(id) sender;
- (void) takeAnotherSelfieTouched:(id)sender;

@end

@protocol EMSmoothAlertViewDelegate <NSObject>

@optional
-(void) alertView:(igTSmoothAlertView *)alertView didDismissWithButton:(UIButton *)button;
-(void) alertViewWillShow:(igTSmoothAlertView *)alertView;
-(void) alertViewDidShow:(igTSmoothAlertView *)alertView;
-(void) alertViewWillDismiss:(igTSmoothAlertView *)alertView;
-(void) alertViewDidDismiss:(igTSmoothAlertView *)alertView;

@end
