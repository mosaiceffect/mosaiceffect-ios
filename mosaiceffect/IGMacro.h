//
//  IGMacro.h
//  IGCafe
//
//  Created by Manish on 07/05/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#ifndef IGCafe_IGMacro_h
#define IGCafe_IGMacro_h

#if DEBUG
#define DLog(format, ...)        NSLog(format, ##__VA_ARGS__)
#else
#define DLog(format, ...)        //
#endif

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_LANDSCAPE  ([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeRight)

#define IS_IPAD_LANDSCAPE (IS_IPAD && IS_LANDSCAPE)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define IPHONE_5_LENGTH  568.0
#define IPHONE_6_LENGTH  667.0
#define IPHONE_6P_LENGTH 736.0
#define IPHONE_7_LENGTH  667.0
#define IPHONE_7P_LENGTH 736.0

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < IPHONE_5_LENGTH)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_5_LENGTH)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_6_LENGTH)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_6P_LENGTH)
#define IS_IPHONE_7 (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_7_LENGTH)
#define IS_IPHONE_7P (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_7P_LENGTH)

#define kSystemVersion              [[[UIDevice currentDevice] systemVersion] floatValue]



#endif
