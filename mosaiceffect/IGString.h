//
//  IGString.h
//  IGCafe
//
//  Created by Manish on 07/05/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#ifndef MyInfogain_IGString_h
#define MyInfogain_IGString_h


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#pragma mark - User Defaults Keys

#define kKeyUserDefaultsAcceptTerms                     @"acceptTerms"
#define kKeyUserDefaultsFirstTimeLogin                  @"firstTimeLogin"
#define kKeyUserDefaultsIsDeviceTokenRegistered         @"isDeviceTokenRegistered"
#define kKeyUserDefaultsIsUserLoggedIn                  @"isUserLoggedIn"
#define kKeyUserDefaultsEmployeeName                    @"employeeName"
#define kKeyUserDefaultsEmployeeID                      @"employeeID"
#define kKeyUserDefaultsEmployeeEmail                   @"employeeEmail"
#define kKeyUserDefaultsDomainID                        @"domainId"
#define kKeyUserDefaultsDeviceToken                     @"deviceToken"
#define kKeyUserDefaultsLastNotificationID              @"lastNotificationID"
#define kKeyUserDefaultsLoginToken                      @"loginToken"
#define kStringLogOut                                   @"Log Out"

#pragma mark - Strings

#define kStringEmptyString                  @""
#define kStringAlert                        @"Alert"
#define kStringOK                           @"OK"
#define kStringCancel                       @"Cancel"
#define KTakeAnotherSelfie                  /*@"Try again"*/@"Take Another Selfie"
#define kStringSignOut                      @"Sign Out"
#define kStringSignIn                       @"Sign In"
#define kStringDelete                       @"Delete"
#define kStringLoginError                   @"Login Error"
#define kStringServerError                   @"Server Error"


#pragma mark - String Message

#define kStringMessageInvalidResponseFromServer                                 @"Invalid response from server"
#define kStringMessageCouldNotLoadVC_FromStoryboard_                            @"Could not get a view controller for identifier: %@ from Storyboard: %@"
#define kStringCouldNotGetStoryBoard                                            @"Could not get storyboard"
#define kStringMessagePleaseEnterAUsername                                      @"Please enter a Username"
#define kStringMessagePleaseEnterAPassword                                      @"Please enter a Password"
#define kStringMessageFailedToAllocateMemory                                    @"Failed to allocate memory"
#define kStringMessageFileNotFoundInBundle_                                     @"File not found in bundle: %@.html"
#define kStringMessageYouNeedToAgreeToTermsAndConditions                        @"You need to agree to the terms and conditions to use the application"
#define kStringInternetConnectionFailure      @"Unable to connect to server. Please try again later."

#pragma mark - Keys

#define kKeyCFBundleShortVersionString      @"CFBundleShortVersionString"


#pragma mark - Image

#define kImageMyInfoAlertIcon                @"myInfo_alertImage"


#define kStringCannotOpenURL                @"Cannot open url"
#define kStringError                        @"Error"
#define kStringUpdate                       @"Update"


#pragma mark - Font Name
#define kFontHelveticaNeue                      @"HelveticaNeue"
#define kNavigationBarColor   [UIColor colorWithRed:223.0/255.0 green:108.0/255.0 blue:60.0/255.0 alpha:1]

#endif
