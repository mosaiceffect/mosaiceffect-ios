//
//  ViewController.m
//  ZLSwipeableViewDemo
//
//  Created by Zhixuan Lai on 11/1/14.
//  Copyright (c) 2014 Zhixuan Lai. All rights reserved.
//

#import "ZLSwipeableViewController.h"
#import "UIColor+FlatColors.h"
#import "CardView.h"
#import "MyCard.h"
#import "AppDelegate.h"
#import "igTSmoothAlertView.h"
#import "IGString.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "PhotoScreenVC.h"
#import "IGMacro.h"


@interface ZLSwipeableViewController ()<EMSmoothAlertViewDelegate>
{
    AppDelegate *appdelegate;
    igTSmoothAlertView *alert;
}

@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic) NSUInteger colorIndex;

@property (nonatomic) BOOL loadCardFromXib;

@property (nonatomic, strong) UIBarButtonItem *reloadBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *leftBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *upBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *rightBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *downBarButtonItem;
@end

@implementation ZLSwipeableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    appdelegate = kAppDelegate;

    self.navigationController.toolbarHidden = YES;
    self.view.clipsToBounds = YES;
    self.view.backgroundColor = [UIColor whiteColor];

    // Do any additional setup after loading the view, typically from a nib.
    self.colorIndex = 0;
    _loadCardFromXib= 1;


    ZLSwipeableView *swipeableView = [[ZLSwipeableView alloc] initWithFrame:CGRectZero];
    self.swipeableView = swipeableView;
    [self.view addSubview:self.swipeableView];

    // Required Data Source
    self.swipeableView.dataSource = self;

    // Optional Delegate
    self.swipeableView.delegate = self;

    self.swipeableView.translatesAutoresizingMaskIntoConstraints = NO;

    NSDictionary *metrics = @{};


    
    
    
    if(IS_IPHONE_6)
    {
        [self.view addConstraints:[NSLayoutConstraint
                                   constraintsWithVisualFormat:@"|-40-[swipeableView]-45-|"
                                   options:0
                                   metrics:metrics
                                   views:NSDictionaryOfVariableBindings(
                                                                        swipeableView)]];
        
        [self.view addConstraints:[NSLayoutConstraint
                                   constraintsWithVisualFormat:@"V:|-220-[swipeableView]-120-|"
                                   options:0
                                   metrics:metrics
                                   views:NSDictionaryOfVariableBindings(
                                                                        swipeableView)]];
    }
       
    else if(IS_IPHONE_6P)
    {
        [self.view addConstraints:[NSLayoutConstraint
                                   constraintsWithVisualFormat:@"|-61-[swipeableView]-63-|"
                                   options:0
                                   metrics:metrics
                                   views:NSDictionaryOfVariableBindings(
                                                                        swipeableView)]];
        
        [self.view addConstraints:[NSLayoutConstraint
                                   constraintsWithVisualFormat:@"V:|-253-[swipeableView]-153-|"
                                   options:0
                                   metrics:metrics
                                   views:NSDictionaryOfVariableBindings(
                                                                        swipeableView)]];
    }
    else
    {
        [self.view addConstraints:[NSLayoutConstraint
                                  constraintsWithVisualFormat:@"|-15-[swipeableView]-15-|"
                                                      options:0
                                                      metrics:metrics
                                                        views:NSDictionaryOfVariableBindings(
                                                                  swipeableView)]];

        [self.view addConstraints:[NSLayoutConstraint
                                  constraintsWithVisualFormat:@"V:|-160-[swipeableView]-79-|"
                                                      options:0
                                                      metrics:metrics
                                                        views:NSDictionaryOfVariableBindings(
                                                                  swipeableView)]];
        
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.imageArray.count > 1)
    {
        self.swipeableView.numberOfActiveViews = 3;
    }
    [self.swipeableView layoutSubviews];

    //[self.swipeableView layoutIfNeeded];
}


- (void)viewDidLayoutSubviews {
    [self.swipeableView loadViewsIfNeeded];
}
#pragma mark - Action

- (void)handleLeft:(UIBarButtonItem *)sender {
    [self.swipeableView swipeTopViewToLeft];
}

- (void)handleRight:(UIBarButtonItem *)sender {
    [self.swipeableView swipeTopViewToRight];
}

- (void)handleUp:(UIBarButtonItem *)sender {
    [self.swipeableView swipeTopViewToUp];
}

- (void)handleDown:(UIBarButtonItem *)sender {
    [self.swipeableView swipeTopViewToDown];
}

- (void)handleReload:(UIBarButtonItem *)sender {
    UIActionSheet *actionSheet =
        [[UIActionSheet alloc] initWithTitle:@"Load Cards"
                                    delegate:self
                           cancelButtonTitle:@"Cancel"
                      destructiveButtonTitle:nil
                           otherButtonTitles:@"Programmatically", @"From Xib", nil];
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    self.loadCardFromXib = buttonIndex == 1;
    self.colorIndex = 0;
    [self.swipeableView discardAllViews];
    [self.swipeableView loadViewsIfNeeded];
}

#pragma mark - ZLSwipeableViewDelegate

- (void)swipeableView:(ZLSwipeableView *)swipeableView
         didSwipeView:(UIView *)view
          inDirection:(ZLSwipeableViewDirection)direction {
    NSLog(@"did swipe in direction: %zd", direction);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView didCancelSwipe:(UIView *)view {
    NSLog(@"did cancel swipe");
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
  didStartSwipingView:(UIView *)view
           atLocation:(CGPoint)location {
    NSLog(@"did start swiping at location: x %f, y %f", location.x, location.y);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
          swipingView:(UIView *)view
           atLocation:(CGPoint)location
          translation:(CGPoint)translation {
    NSLog(@"swiping at location: x %f, y %f, translation: x %f, y %f", location.x, location.y,
          translation.x, translation.y);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
    didEndSwipingView:(UIView *)view
           atLocation:(CGPoint)location {
    NSLog(@"did end swiping at location: x %f, y %f", location.x, location.y);
}

#pragma mark - ZLSwipeableViewDataSource

- (UIView *)nextViewForSwipeableView:(ZLSwipeableView *)swipeableView {
    self.imageArray = appdelegate.arrAllImages;

    if (self.colorIndex >= self.imageArray.count) {
        self.colorIndex = 0;
    }

    CardView *view = [[CardView alloc] initWithFrame:swipeableView.bounds];
    MyCard *contentView = nil;

    if (self.loadCardFromXib) {
        
        contentView =
        [[NSBundle mainBundle] loadNibNamed:@"MyCard" owner:self options:nil][0];
        contentView.translatesAutoresizingMaskIntoConstraints = NO;
   
        contentView.clipsToBounds = true;
        contentView.btnShareImage.tag = self.colorIndex;
        contentView.btnCamera.tag = self.colorIndex;
        contentView.btnUploadImage.tag = self.colorIndex;
        contentView.cmdDelegate = self;
        
        if(self.colorIndex > 0)
        {
            contentView.imgMyPixeleteImage.hidden = false;
            contentView.btnShareImage.hidden = false;
            contentView.btnCamera.hidden = true;
            contentView.btnUploadImage.hidden = true;
            contentView.lblCameraText.hidden = true;
            contentView.lblUploadImageText.hidden = true;
            contentView.imgMyPixeleteImage.image = [self.imageArray objectAtIndex:self.colorIndex];
        }
        else{
            contentView.imgMyPixeleteImage.hidden = true;
            contentView.btnShareImage.hidden = true;
            contentView.btnCamera.hidden = false;
            contentView.btnUploadImage.hidden = false;
            
            contentView.lblCameraText.hidden = false;
            contentView.lblUploadImageText.hidden = false;
        }
        [view addSubview:contentView];
        self.colorIndex++;

        


        // This is important:
        // https://github.com/zhxnlai/ZLSwipeableView/issues/9
        NSDictionary *metrics =
            @{ @"height" : @(view.bounds.size.height),
               @"width" : @(view.bounds.size.width) };
        NSDictionary *views = NSDictionaryOfVariableBindings(contentView);
        [view addConstraints:[NSLayoutConstraint
                                 constraintsWithVisualFormat:@"H:|[contentView(width)]"
                                                     options:0
                                                     metrics:metrics
                                                       views:views]];
        [view addConstraints:[NSLayoutConstraint
                                 constraintsWithVisualFormat:@"V:|[contentView(height)]"
                                                     options:0
                                                     metrics:metrics
                                                       views:views]];
    }
    return view;
}

#pragma mark - ()

- (UIColor *)colorForName:(NSString *)name {
    NSString *sanitizedName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *selectorString = [NSString stringWithFormat:@"flat%@Color", sanitizedName];
    Class colorClass = [UIColor class];
    return [colorClass performSelector:NSSelectorFromString(selectorString)];
}


/*
 Function:onClickCamera:(int)tag
 Description: This function is called when user click on Camera button to open camera for selfie.
 */
-(void)onClickCamera:(int)tag
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = NO;
        imagePicker.cameraDevice=UIImagePickerControllerCameraDeviceFront;
        
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }else{
        
        alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:@"Camera Unavailable"
                                                           andText:@"Unable to find a camera on your device"
                                                   andCancelButton:NO
                                                          andColor:[UIColor colorWithRed:(32/255.0) green:(122/255.0) blue:(204/255.0) alpha:1.0]
                                                          andImage:[UIImage imageNamed:@"myInfo_alertImage"]
                                                           andFont:[UIFont fontWithName:kFontHelveticaNeue size:14.0f]];
        alert.delegate = self;
        [alert show];
        
    }
}

/*
 Function:onClickUploadImage:(int)tag
 Description: This function is called when user click on upload image button to open internal  saved photo library.
 */
-(void)onClickUploadImage:(int)tag
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
    else{
        alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:@"Photo Library Unavailable"
                                                           andText:@"Unable to find a photo library on your device"
                                                   andCancelButton:NO
                                                          andColor:[UIColor colorWithRed:(32/255.0) green:(122/255.0) blue:(204/255.0) alpha:1.0]
                                                          andImage:[UIImage imageNamed:@"myInfo_alertImage"]
                                                           andFont:[UIFont fontWithName:kFontHelveticaNeue size:14.0f]];
        alert.delegate = self;
        [alert show];
        
    }

}


/*
 Function:onClickShareImage:(UIImage*)image
 Description: This function is called to share image .
 */
-(void)onClickShareImage:(UIImage*)image
{
    NSLog(@"uploadCamera");
    NSArray * items = @[image];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    [self presentViewController:controller animated:YES completion:nil];
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary)
    {
        //retrieve the actual UIImage
        UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
        //Or you can get the image url from AssetsLibrary
        [self pushImageDetailView:image];
        
    }
    if(picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
        
        if([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
            UIImage *photoTaken = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            
            //Save Photo to library only if it wasnt already saved i.e. its just been taken
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                //UIImageWriteToSavedPhotosAlbum(photoTaken, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
            }
            [self pushImageDetailView:photoTaken];
            
            
            
        };
        
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}



-(void)pushImageDetailView:(UIImage*)image
{
    //To Do Open Image Detail ViewController
    
    PhotoScreenVC * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"photoScreenController"];
    controller.selectedImage = image;

    [self.navigationController pushViewController:controller animated:YES];
    
}


@end
