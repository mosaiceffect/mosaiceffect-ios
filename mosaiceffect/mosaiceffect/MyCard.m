//
//  MyCard.m
//  ZLSwipeableViewDemo
//
//  Created by Sandeep on 20/05/17.
//  Copyright © 2017 Zhixuan Lai. All rights reserved.
//

#import "MyCard.h"

@implementation MyCard

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    // Shadow
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.33;
    self.layer.shadowOffset = CGSizeMake(0, 1.5);
    self.layer.shadowRadius = 4.0;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
   // self.clipsToBounds = true;
    
     //shipra removed corner radius
    //self.layer.cornerRadius = 10.0;
    self.layer.masksToBounds = false;
    self.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor redColor]);
 
}


-(IBAction)onClickCamera:(int)tag
{
    [self.cmdDelegate onClickCamera:tag];
}

-(IBAction)onClickUploadImage:(int)tag
{
    [self.cmdDelegate onClickUploadImage:tag];
}

-(IBAction)onClickShareImage:(int)tag
{
    [self.cmdDelegate onClickShareImage:self.imgMyPixeleteImage.image];
}

-(IBAction)onClickSaveImage:(int)tag
{
    [self.cmdDelegate onClickSaveImage:self.imgMyPixeleteImage.image];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
