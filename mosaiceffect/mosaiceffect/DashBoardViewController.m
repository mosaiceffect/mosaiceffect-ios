//
//  ViewController.m
//  NetApp_Demo_Camera
//
//  Created by Sandeep on 17/05/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

#import "DashBoardViewController.h"
#import "PhotoScreenVC.h"
#import <Photos/Photos.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "igTSmoothAlertView.h"
#import "IGString.h"
#import "AppDelegate.h"
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>


@interface DashBoardViewController ()<EMSmoothAlertViewDelegate,ZLSwipeableViewAnimator>{
    igTSmoothAlertView *alert;
    AppDelegate* appDelegate;
}

@end

@implementation DashBoardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = kAppDelegate;
     [FIRAnalytics logEventWithName:kFIREventViewItem parameters:@{kFIRParameterItemName:@"DashBoardViewController"}];
    self.swipeableView.numberOfActiveViews = (appDelegate.arrAllImages.count > 0)?appDelegate.arrAllImages.count:1;//self.3;
    
    self.swipeableView.numberOfActiveViews = (appDelegate.arrAllImages.count > 1)? 3 : 1;
    self.swipeableView.viewAnimator = self;
    
 
    
    self.topBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"loginBg"]];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
        [self.navigationController.navigationBar setHidden:TRUE];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)animateView:(UIView *)view
              index:(NSUInteger)index
              views:(NSArray<UIView *> *)views
      swipeableView:(ZLSwipeableView *)swipeableView {
    CGFloat degree = sin(0.5 * index);
    NSTimeInterval duration = 0.4;
    CGPoint offset = CGPointMake(0, CGRectGetHeight(swipeableView.bounds) * 0.3);
    CGPoint translation = CGPointMake(degree * 10.0, -(index * 5.0));
    [self rotateAndTranslateView:view
                       forDegree:degree
                     translation:translation
                        duration:duration
              atOffsetFromCenter:offset
                   swipeableView:swipeableView];
}

- (CGFloat)degreesToRadians:(CGFloat)degrees {
    return degrees * M_PI / 180;
}

- (CGFloat)radiansToDegrees:(CGFloat)radians {
    return radians * 180 / M_PI;
}

- (void)rotateAndTranslateView:(UIView *)view
                     forDegree:(float)degree
                   translation:(CGPoint)translation
                      duration:(NSTimeInterval)duration
            atOffsetFromCenter:(CGPoint)offset
                 swipeableView:(ZLSwipeableView *)swipeableView {
    float rotationRadian = [self degreesToRadians:degree];
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         view.center = [swipeableView convertPoint:swipeableView.center
                                                          fromView:swipeableView.superview];
                         CGAffineTransform transform =
                         CGAffineTransformMakeTranslation(offset.x, offset.y);
                         transform = CGAffineTransformRotate(transform, rotationRadian);
                         transform = CGAffineTransformTranslate(transform, -offset.x, -offset.y);
                         transform =
                         CGAffineTransformTranslate(transform, translation.x, translation.y);
                         view.transform = transform;
                     }
                     completion:nil];
}








@end
