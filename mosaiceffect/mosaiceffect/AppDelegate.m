//
//  AppDelegate.m
//  mosaiceffect
//
//  Created by Shipra Chauhan on 17/05/17.
//  Copyright © 2017 Ankur. All rights reserved.
//

#import "AppDelegate.h"
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>
//#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.arrAllImages = [[NSMutableArray alloc]init];
    [self getAllPictures];
    [FIRApp configure];


    // Override point for customization after application launch.
   /* NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];*/
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)showMainScreen
{
    NSLog(@"show dashboard");
   UIStoryboard *storyboard = self.window.rootViewController.storyboard;
   UINavigationController *controller = (UINavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"dashnav"];
    self.window.rootViewController = controller;
}

-(void)getAllPictures
{
    [self.arrAllImages removeAllObjects];
    [self.arrAllImages addObject:@"UploadImageCard"];

    __block PHAssetCollection *collection;
    
    // Find the album
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@", @"Mosaic"];
    collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                          subtype:PHAssetCollectionSubtypeAny
                                                          options:fetchOptions].firstObject;
    
    PHFetchResult *collectionResult = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
    
    NSMutableArray *assets = [[NSMutableArray alloc] init];
    
    [collectionResult enumerateObjectsUsingBlock:^(PHAsset *asset, NSUInteger idx, BOOL *stop) {
        [assets addObject:asset];
        
    }];
    
    PHImageRequestOptions * requestOptions = [[PHImageRequestOptions alloc] init];
    PHImageManager *manager = [PHImageManager defaultManager];
    
    __block UIImage *ima;
    
    for (PHAsset *asset in assets) {
        [manager requestImageForAsset:asset
                           targetSize:PHImageManagerMaximumSize//CGSizeMake(300, 300)
                          contentMode:PHImageContentModeDefault
                              options:requestOptions
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            ima = image;
                                [self.arrAllImages addObject:ima];
                            NSLog(@"images %@", self.arrAllImages);
                        }];
        
    }
    
}




@end
