//
//  AppDelegate.h
//  mosaiceffect
//
//  Created by Shipra Chauhan on 17/05/17.
//  Copyright © 2017 Ankur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>


#define kAppDelegate                (AppDelegate *)[[UIApplication sharedApplication] delegate]

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray *arrAllImages;
- (void) showMainScreen;
-(void)getAllPictures;


@end

