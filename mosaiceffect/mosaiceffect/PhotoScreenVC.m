//
//  PhotoScreenVC.m
//  mosaiceffect
//
//  Created by Parul on 17/05/17.
//  Copyright © 2017 Ankur. All rights reserved.
//

#import "PhotoScreenVC.h"
#import "SVProgressHUD.h"
#import "SphereMenu.h"
#import <Social/Social.h>
#import "igTSmoothAlertView.h"
#import <PhotosUI/PhotosUI.h>
#import "IGString.h"
#import "AppDelegate.h"
#import "IGString.h"
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import <QuartzCore/QuartzCore.h>
#import "QuartzCore/CALayer.h"

#define EYE_SIZE_RATE 0.3f
#define MOUTH_SIZE_RATE 0.4f
#define DEGREES_TO_RADIANS(degrees)((M_PI * degrees)/180)

@interface PhotoScreenVC () <EMSmoothAlertViewDelegate>{
      igTSmoothAlertView *alert;
}

@property (weak, nonatomic) IBOutlet UIImageView *finalImageView;
@property (weak, nonatomic) IBOutlet UIView *randomImagesView;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *mosaiceBtn;
@property (weak, nonatomic) IBOutlet UIView *hashtagLabel;
@property (strong,nonatomic) UIImage *shareImage;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UIView *smallView;
@property (weak, nonatomic) AppDelegate* appDelegate;
- (IBAction)mosaiceBtnClkd:(UIButton *)sender;

@end

@implementation PhotoScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.appDelegate = kAppDelegate;
   
    // Do any additional setup after loading the view.
    self.finalImageView.image = self.selectedImage;
 //   self.hashtagLabel.hidden = YES;

    
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"top_bar"]]];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"backicon"] style:UIBarButtonItemStyleDone target:self action:@selector(actionBackButton)];
    barButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    //self.navigationItem.title = @"NetApp";
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}]; // set title color
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];

    //[self addSocialShareView];
}


- (IBAction)mosaiceBtnClkd:(UIButton *)sender {
    self.bottomOptionsBar.hidden = true;
        [self setMosaiceEffect];
}

-(void)setMosaiceEffect{
   // _overlayView.alpha = 0 ;
    [SVProgressHUD showWithStatus:@"Detecting Face"];
    [self detectFacesNew:self.finalImageView];

}


-(IBAction)actionBackButton
{
    [self.navigationController popViewControllerAnimated:true];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated{
    self.mosaiceBtn.hidden=NO;
    self.saveBtn.hidden=YES;
    self.shareBtn.hidden=YES;
    //shipra
    //self.mosaiceBtn.layer.cornerRadius = 5;
    //self.saveBtn.layer.cornerRadius = self.saveBtn.frame.size.height/2;
    //self.shareBtn.layer.cornerRadius =  self.shareBtn.frame.size.height/2;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
     [SVProgressHUD dismiss];
}



- (IBAction)saveImageToPhone:(id)sender {
    
    if (self.shareImage) {
        [self saveimageToCustomAlbum : self.shareImage];
        [self image:self.shareImage didFinishSavingWithError:nil contextInfo:nil];
    }
    else{
        [self captureView:self.screenshotview completion:^(UIImage *image) {
            self.shareImage = image;
            [self saveimageToCustomAlbum : self.shareImage];
            [self image:self.shareImage didFinishSavingWithError:nil contextInfo:nil];
        }];
    }
}


-(void) showWithDelay
{
    self.mosaiceBtn.hidden=YES;
    self.saveBtn.hidden=NO;
    self.shareBtn.hidden=NO;
    self.bottomOptionsBar.hidden = false;
}

- (IBAction)shareImageViaSocail:(id)sender {
    if (self.shareImage) {
        NSArray * items = @[self.shareImage];
        UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
        [self presentViewController:controller animated:YES completion:nil];
    }
    else{
    
        [self captureView:self.screenshotview completion:^(UIImage *image) {
            self.shareImage = image;
            NSArray * items = @[self.shareImage];
            UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
            [self presentViewController:controller animated:YES completion:nil];
        }];
    }
}


- (void)image:(UIImage *)image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    
        if (!error)
        {
            alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:@"Success!"
                                                               andText:@"The picture has been saved successfully"
                                                       andCancelButton:YES
                                                              andColor:[UIColor colorWithRed:(32/255.0) green:(122/255.0) blue:(204/255.0) alpha:1.0]
                                                              andImage:[UIImage imageNamed:@"myInfo_alertImage"]
                                                               andFont:[UIFont fontWithName:kFontHelveticaNeue size:11.0f]];
            
        }
        
        else
        {
            alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:@"Error!"
                                                               andText:@"Error in saving picture"
                                                       andCancelButton:NO
                                                              andColor:[UIColor colorWithRed:(32/255.0) green:(122/255.0) blue:(204/255.0) alpha:1.0]
                                                              andImage:[UIImage imageNamed:@"myInfo_alertImage"]
                                                               andFont:[UIFont fontWithName:kFontHelveticaNeue size:14.0f]];
        }
        alert.delegate = self;
        [alert show];
}

/*-(void) addSocialShareView{
    
    UIImage *startImage = [UIImage imageNamed:@"shared"];
    UIImage *image1 = [UIImage imageNamed:@"icon-twitter"];
    UIImage *image2 = [UIImage imageNamed:@"icon-email"];
    UIImage *image3 = [UIImage imageNamed:@"icon-facebook"];
    NSArray *images = @[image1, image2, image3];
    SphereMenu *sphereMenu = [[SphereMenu alloc] initWithStartPoint:self.shareBtn.center
                                                         startImage:startImage
                                                      submenuImages:images];
    self.shareBtn.hidden = YES;
    sphereMenu.sphereDamping = 0.3;
    sphereMenu.sphereLength = 85;
    sphereMenu.delegate = self;
    [_smallView addSubview:sphereMenu];
}


- (void)sphereDidSelected:(int)index
{
    
    
    if (index == 0){
        
        SLComposeViewController * fbVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [fbVC addImage:_finalImageView.image];
        
        
        [fbVC setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:fbVC animated:YES completion:nil];
    }
    
    if (index == 2){
        
        SLComposeViewController * fbVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [fbVC addImage:_finalImageView.image];
        
        
        [fbVC setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:fbVC animated:YES completion:nil];
    }
    
   

    NSLog(@"sphere %d selected", index);
}*/
-(void) alertView:(igTSmoothAlertView *)alertView didDismissWithButton:(UIButton *)button{
     [self.navigationController popViewControllerAnimated:YES];

}

-(void)saveimageToCustomAlbum:(UIImage *)image{
    
        
        __block PHFetchResult *photosAsset;
        __block PHAssetCollection *collection;
        __block PHObjectPlaceholder *placeholder;
        
        // Find the album
        PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
        fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@", @"Mosaic"];
        collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                              subtype:PHAssetCollectionSubtypeAny
                                                              options:fetchOptions].firstObject;
        // Create the album
        if (!collection)
        {
            [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                PHAssetCollectionChangeRequest *createAlbum = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:@"Mosaic"];
                placeholder = [createAlbum placeholderForCreatedAssetCollection];
            } completionHandler:^(BOOL success, NSError *error) {
                if (success)
                {
                    PHFetchResult *collectionFetchResult = [PHAssetCollection fetchAssetCollectionsWithLocalIdentifiers:@[placeholder.localIdentifier]
                                                                                                                options:nil];
                    collection = collectionFetchResult.firstObject;
                }
            }];
        }
        
        // Save to the album
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            NSData *imgData = UIImageJPEGRepresentation(image, 0.9);
            PHAssetChangeRequest *assetRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:[UIImage imageWithData:imgData]];
            placeholder = [assetRequest placeholderForCreatedAsset];
            photosAsset = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
            PHAssetCollectionChangeRequest *albumChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection
                                                                                                                          assets:photosAsset];
            [albumChangeRequest addAssets:@[placeholder]];
        } completionHandler:^(BOOL success, NSError *error) {
            if (success)
            {
                [self.appDelegate.arrAllImages addObject:image];
            }
            else
            {
                NSLog(@"%@", error);
            }
        }];
        

    
   }

- (void)detectFacesNew :(UIView *) view
{
    // draw a CI image with the previously loaded face detection picture
    self.finalImageView.hidden = NO;
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *contextimage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CIImage* image = [CIImage imageWithCGImage:contextimage.CGImage];
    // create a face detector - since speed is not an issue now we'll use a high accuracy detector
    CIDetector* detector = [CIDetector detectorOfType:CIDetectorTypeFace
                                              context:nil options:[NSDictionary dictionaryWithObject:CIDetectorAccuracyLow forKey:CIDetectorAccuracy]];
    
    // create an array containing all the detected faces from the detector
    NSArray* features = [detector featuresInImage:image];
    
    
    // CoreImage coordinate system origin is at the bottom left corner and UIKit is at the top left corner
    // So we need to translate features positions before drawing them to screen
    // In order to do so we make an affine transform
    // **Note**
    // Its better to convert CoreImage coordinates to UIKit coordinates and
    // not the other way around because doing so could affect other drawings
    // i.e. In the original sample project you see the image and the bottom, Isn't weird?
    CGAffineTransform transform = CGAffineTransformMakeScale(1, -1);
    transform = CGAffineTransformTranslate(transform, 0, -self.view.bounds.size.height);
    
    // we'll iterate through every detected face.  CIFaceFeature provides us
    // with the width for the entire face, and the coordinates of each eye
    // and the mouth if detected.  Also provided are BOOL's for the eye's and
    // mouth so we can check if they already exist.
    for(CIFaceFeature* faceFeature in features)
    {
        
        [SVProgressHUD showSuccessWithStatus:@"Face Detected "];

        // Get the face rect: Translate CoreImage coordinates to UIKit coordinates
        const CGRect faceRect = CGRectApplyAffineTransform(faceFeature.bounds, transform);
       // [self startWeavingForRect:faceRect];
        //CGRect  newresultFrame = self.view.convert(randomRect, from: yellowView);
        //CGRect newRect = [self.finalImageView convertRect:faceRect toView:self.view];
      //  NSLog(@"oldFacebound %@",NSStringFromCGRect(faceRect));
      //  NSLog(@"newfacebound %@",NSStringFromCGRect(newRect));
        // create a UIView using the bounds of the face
        UIView* faceView = [[UIView alloc] initWithFrame:faceRect];
        //faceView.layer.borderWidth = 1;
        //faceView.layer.borderColor = [[UIColor redColor] CGColor];
        [self startWeavingEffectForView:faceView];
        // get the width of the face
        //CGFloat faceWidth = faceFeature.bounds.size.width;
        // add the new view to create a box around the face
       // [self.finalImageView addSubview:faceView];
    }
    if(features.count == 0) {
        [SVProgressHUD showErrorWithStatus:@"No face Detected "];
        [self createSimpleMosaicView];
    }
}

-(double)distanceToView:(UIView *)view :(UIView *) view2
{
    return sqrt(pow(view.center.x - view2.center.x, 2) + pow(view.center.y - view2.center.y, 2));
}


-(void) startWeavingEffectForView :(UIView *) faceView {
    [SVProgressHUD showWithStatus:@"Processing Images"];
    dispatch_queue_t myQueue = dispatch_queue_create("MosaicQueue",NULL);
    
    dispatch_async(myQueue, ^{
        int noOfTimes = 6;
        for (int i = 0 ; i < 360*noOfTimes; i++)
        {
            int degree = i%360;
            float R = arc4random_uniform(self.view.frame.size.height)+1;
            float center_X = faceView.center.x;
            float center_Y = faceView.center.y;
            
            float phi = DEGREES_TO_RADIANS(degree);
            float X = R * cos (phi) + center_X;
            float Y = R * sin (phi) + center_Y;
            
            int randomnumber = arc4random_uniform(109)+1;
            __block UIImage *randomImage = [UIImage imageNamed:[NSString stringWithFormat:@"Image_%i.jpg",randomnumber]];
            if (randomImage == nil) {
                NSLog(@"check nil image with randomnumber  %i",randomnumber);
            }
            
            float imageWidth = (randomImage.size.width/25)*(R/20+1);
            float imageHeight = (randomImage.size.height/25)*(R/20+1);
            
            CGRect  imageViewRect = CGRectMake(X, Y, imageWidth,imageHeight);
            
            if (CGRectContainsPoint(faceView.frame, CGPointMake(X,Y)))
            {
                continue;
            }
            
            __block UIImageView * imageView = [[UIImageView alloc] initWithFrame:imageViewRect];
            float distance = [self distanceToView:faceView :imageView];
            imageView.alpha = distance/400.0;
            imageView.image = randomImage;
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            imageView.layer.shadowRadius = 3;
            imageView.layer.shadowOffset = CGSizeZero;;
            imageView.layer.shadowColor = [UIColor blackColor].CGColor;
            imageView.layer.shadowOpacity = 1;
            imageView.clipsToBounds = NO;
            imageView.backgroundColor  = [UIColor redColor];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSUInteger viewcount =  self.randomImagesView.subviews.count;
                for (int j = 0; j<viewcount; j++) {
                    UIView *oldImageView = [self.randomImagesView.subviews.mutableCopy objectAtIndex:j];
                    if (CGRectIntersectsRect(oldImageView.frame, imageView.frame))
                    {
                        //imageView.backgroundColor=[UIColor redColor];
                        imageView = nil;
                        randomImage = nil;
                        continue;
                        //imageView.hidden = YES;
                    }
                }
                [self.randomImagesView addSubview:imageView];
            });
            
        }
    });
    
    dispatch_async(myQueue, ^{
        CGFloat  width = self.view.frame.size.width/30;
        CGFloat  height = width*0.75;
        int  noofcolumns = self.view.frame.size.width/width+1;
        int  noofrows = self.view.frame.size.height/height+1;
        for (int i = 0; i<noofrows; i++) {
            for (int j = 0; j<noofcolumns; j++) {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(j*width, i*height, width, height)];
                float distance = [self distanceToView:faceView :imageView];
                imageView.alpha = distance/400.0;
                if (imageView.alpha<0.2) {
                    imageView.alpha = 0.2;
                }
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                int randomnumber = arc4random_uniform(109)+1;
                UIImage *randomImage = [UIImage imageNamed:[NSString stringWithFormat:@"Image_%i.jpg",randomnumber]];
                imageView.image = randomImage;
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSUInteger viewcount =  self.randomImagesView.subviews.count;
                    for (int j = 0; j<viewcount; j++) {
                        UIView *oldImageView = [self.randomImagesView.subviews objectAtIndex:j];
                        if (CGRectIntersectsRect(oldImageView.frame, imageView.frame))
                        {
                            //continue;
                            if (CGRectContainsRect(oldImageView.frame, imageView.frame))
                            {
                                imageView.hidden = YES;
                            }
                            //                            else{
                            //                                imageView.hidden = NO;
                            //                            }
                        }
                    
                        if ((i==noofrows-1) && (j == noofcolumns-1)) {
                            [SVProgressHUD showSuccessWithStatus:@"Processing Completed"];
                            [self showWithDelay];
                        }
                    }
                    
                    
                    // Update the UI
                    [self.overlayView addSubview:imageView];
                   // [self.view bringSubviewToFront:self.randomMosaicView];
                    
                });
                //   NSLog(@"imageView.frame %@",NSStringFromCGRect(imageView.frame));
            }
        }
    });
    
}

-(void) createSimpleMosaicView
{
    [SVProgressHUD showWithStatus:@"Processing Images"];

    dispatch_queue_t myQueue = dispatch_queue_create("MosaicQueue",NULL);
    dispatch_async(myQueue, ^{
        // Perform long running process
        CGFloat  width = self.view.frame.size.width/20;
        CGFloat  height = width*0.75;
        int  noofcolumns = self.view.frame.size.width/width+1;
        int  noofrows = self.view.frame.size.height/height+1;
          dispatch_async(dispatch_get_main_queue(), ^{

        for (int i = 0; i<noofrows; i++) {
            for (int j = 0; j<noofcolumns; j++) {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(j*width, i*height, width, height)];
                imageView.alpha= 0.4;
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                int randomnumber = arc4random_uniform(109)+1;
                UIImage *randomImage = [UIImage imageNamed:[NSString stringWithFormat:@"Image_%i.jpg",randomnumber]];
                imageView.image = randomImage;
                    // Update the UI
                    [self.overlayView addSubview:imageView];
                    if ((i==noofrows-1) && (j == noofcolumns-1)) {
                        [self showWithDelay];
                        [SVProgressHUD showSuccessWithStatus:@"Processing Completed"];
                    }
                //   NSLog(@"imageView.frame %@",NSStringFromCGRect(imageView.frame));
            }
            
        }
        });

    });
    
    
}

-(void)captureView :(UIView *) view
                   completion:(void (^)(UIImage* image))handler
{
    [SVProgressHUD showWithStatus:@"Generating Image"];
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         
         if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
             UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, [UIScreen mainScreen].scale);
         } else {
             UIGraphicsBeginImageContext(self.view.bounds.size);
         }
         [view.layer renderInContext:UIGraphicsGetCurrentContext()];
         UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
         UIGraphicsEndImageContext();

         dispatch_async(dispatch_get_main_queue(), ^{
             if (image) {
                // [SVProgressHUD showSuccessWithStatus:@"Image is Ready!!"];
                 [SVProgressHUD dismiss];
             }
             else{
                 [SVProgressHUD showErrorWithStatus:@"Proccessing Failed"];
             }
             handler(image);
         });
     });
}

@end
