//
//  PhotoScreenVC.h
//  mosaiceffect
//
//  Created by Parul on 17/05/17.
//  Copyright © 2017 Ankur. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PhotoScreenVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *screenshotview;
@property(strong,nonatomic) UIImage *selectedImage;
@property (weak, nonatomic) IBOutlet UIImageView *overlayView;
@property (weak, nonatomic) IBOutlet UIView *bottomOptionsBar;

-(void)actionBackButton;



@end
