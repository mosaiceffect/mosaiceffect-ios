//
//  main.m
//  mosaiceffect
//
//  Created by Shipra Chauhan on 17/05/17.
//  Copyright © 2017 Ankur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
