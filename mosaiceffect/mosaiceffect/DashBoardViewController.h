//
//  ViewController.h
//  NetApp_Demo_Camera
//
//  Created by Sandeep on 17/05/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLSwipeableViewController.h"


@interface DashBoardViewController : ZLSwipeableViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{

}
@property(nonatomic,weak)IBOutlet UIView* viewDashboard;
@property(nonatomic,weak)IBOutlet UIImageView* viewImageFrame;
@property(nonatomic,weak)IBOutlet UIView* viewButtonFrame;
@property(nonatomic,weak)IBOutlet UIView* topBar;
@property(nonatomic,weak)IBOutlet UIButton* btnCamera;
@property(nonatomic,weak)IBOutlet UIButton* btnUploadImage;

@end

