//
//  MyCard.h
//  ZLSwipeableViewDemo
//
//  Created by Sandeep on 20/05/17.
//  Copyright © 2017 Zhixuan Lai. All rights reserved.
//

#import <UIKit/UIKit.h>

// DataSource
@protocol MyCardViewdelegate <NSObject>

-(void)onClickCamera:(int)tag;
-(void)onClickUploadImage:(int)tag;
-(void)onClickShareImage:(UIImage*)image;
-(void)onClickSaveImage:(UIImage*)image;
@end


@interface MyCard : UIView


@property(nonatomic,weak)IBOutlet UIImageView* imgMyPixeleteImage;
@property(nonatomic,weak)IBOutlet UIButton* btnCamera;
@property(nonatomic,weak)IBOutlet UIButton* btnUploadImage;
@property(nonatomic,weak)IBOutlet UIButton* btnSaveImage;
@property(nonatomic,weak)IBOutlet UIButton* btnShareImage;
@property(nonatomic,weak)IBOutlet UILabel* lblCameraText;
@property(nonatomic,weak)IBOutlet UILabel* lblUploadImageText;


@property(nonatomic,weak)id<MyCardViewdelegate>cmdDelegate;

@end
