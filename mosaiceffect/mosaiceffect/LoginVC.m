//
//  LoginVC.m
//  myinfogain
//
//  Created by Shipra Chauhan on 18/04/17.
//  Copyright © 2017 Ankur. All rights reserved.
//

#import "LoginVC.h"
#import "AppDelegate.h"
#import "igTSmoothAlertView.h"
#import "HyLoglnButton.h"
#import "IGMacro.h"
#import "igTSmoothAlertView.h"
#import "IGString.h"


#define MAX_CHARACTER_LENGTH        50
#define TAG_AUTHENTICATION_FAILURE_ALERT          5556
#define TAG_ALERT_LOGIN_ERROR       5555
#define TAG_ALERT_UPDATE                2222

@interface LoginVC () <EMSmoothAlertViewDelegate>{
    BOOL checkingForUpdate;
    BOOL updateAvailable;
    
    BOOL shouldShowUpdateAlert;
    
    NSString *updateResponseTitle;
    NSString *updateResponseMessage;
    NSString *updateResponseURL;
    igTSmoothAlertView *alert;

}

@property (weak, nonatomic) IBOutlet HyLoglnButton *signInBtn;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIView *cloudView;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewUserImage;

- (IBAction)signInBtnClkd:(HyLoglnButton *)sender;


@end

@implementation LoginVC{
    AppDelegate *appdelegate;
}




- (void)viewDidLoad {
    [super viewDidLoad];
  //  [self getAllPictures];
    appdelegate = kAppDelegate;
    self.backgroundImage.hidden = NO;
    self.backgroundImage.alpha = 1.0f;
    

    //self.signInBtn.layer.cornerRadius = 20;
    //self.signInBtn.clipsToBounds = YES;
   }



-(void)viewDidAppear:(BOOL)animated{
    

}

- (void)viewDidDisappear:(BOOL)animated{
   /* self.cloudViewLeadingConstraint.constant = 0;
    [self.cloudsView.layer removeAllAnimations];
    [self.view layoutIfNeeded];*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)signInBtnClkd:(HyLoglnButton *)sender{
   
    //[self.signInBtn ExitAnimationCompletion:^{

            [appdelegate showMainScreen];
       // }];
}



-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}




@end
